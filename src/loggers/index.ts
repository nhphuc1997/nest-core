import * as winstonDailyRotateFile from 'winston-daily-rotate-file';
import { WinstonModule } from 'nest-winston';
import { ConfigService } from '@nestjs/config';

const configService = new ConfigService();
const dirname = configService.get<string>('DIR_NAME');

const transports = {
  combinedFile: new winstonDailyRotateFile({
    dirname: dirname,
    filename: 'info',
    extension: '.log',
    level: 'info',
  }),
  errorFile: new winstonDailyRotateFile({
    dirname: dirname,
    filename: 'error',
    extension: '.log',
    level: 'error',
  }),
};

export const winstonInstance = WinstonModule.createLogger({
  transports: [transports.combinedFile, transports.errorFile],
});
