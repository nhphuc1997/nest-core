import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ClientDocument = Client & Document;

@Schema()
export class Client {
  @Prop()
  name: string;

  @Prop()
  code: string;

  @Prop()
  master_key: string;

  @Prop()
  status: string;

  @Prop()
  call_service_count: number;

  @Prop()
  created_by: string;

  @Prop({ type: Date })
  created_at: string;

  @Prop({ type: Date })
  updated_at: string;

  @Prop()
  updated_by: string;
}

export const ClientSchema = SchemaFactory.createForClass(Client);
