import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { dbConfig } from 'src/mongodb/db-config';
import { comparePassword, checkSum, encryptData, decryptData } from 'src/utils';
import * as sc from 'src/configs/service_config.json';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { PayloadSendEventToCDP } from 'src/validators/payload-validator';
import { HandleResponseService } from 'src/handle-response/handle-response.service';

@Injectable()
export class HandleRequestService {
  private collectionClient = dbConfig.collection.CLIENT;
  private authConsumerUrl = `${sc.authentication_service.host}/authentication/post_consumer_request`;

  constructor(
    @InjectConnection()
    private readonly connection: Connection,
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
    private readonly hanldeResponseService: HandleResponseService,
  ) {}

  private async findClientByCode(clientCode: string) {
    return await this.connection.collection(this.collectionClient).findOne({
      CODE: clientCode,
    });
  }

  private async updateClientCallServiceCount(client: any) {
    return await client.update({
      CALL_SERVICE_COUNT: client.CALL_SERVICE_COUNT + 1,
    });
  }

  async requestIn(payload: any) {
    const { checksum, raw_data, client_code, client_secret } = payload;

    if (client_code === null || client_secret === null) return null;

    const client = await this.findClientByCode(client_code);

    if (client === null) return null;

    const { MASTER_KEY, STATUS, _id, SECRET } = client;
    const verifyPassword = comparePassword(client_secret, SECRET);
    const verifyCheckSum = checkSum(JSON.stringify(raw_data)) === checksum;

    if (STATUS !== 1 || verifyPassword === false || verifyCheckSum === false) return null;

    this.updateClientCallServiceCount(client);

    if (raw_data === null) return { client_code: client_code };

    return {
      id: _id,
      raw_data,
      client_code,
      master_key: MASTER_KEY,
      client_secret: SECRET,
    };
  }

  async requestClient(payload: any) {
    const { client_code, checksum, encrypted_data } = payload;
    const client = await this.findClientByCode(client_code);

    if (!client) return null;
    const { MASTER_KEY, STATUS } = client;
    if (STATUS !== 1) return null;
    if (checkSum(encrypted_data) !== checksum) return null;

    const decryptedData = decryptData(encrypted_data, MASTER_KEY);

    return {
      decrypted_data: decryptedData,
      master_key: MASTER_KEY,
    };
  }

  async requestCustomer(payload: any) {
    try {
      const { checksum, encrypted_data, device_unique_id } = payload;
      const verifyCheckSum = checkSum(encrypted_data) === checksum;

      if (verifyCheckSum === false) return null;

      // make data for authenticated
      const authData = {
        encrypted_data: encrypted_data,
        client_main_mobile_app_code: sc.client_main_mobile_app_code,
        device_unique_id: device_unique_id,
      };

      const preResult = await this.outECO(this.authConsumerUrl, authData, 'CPS');

      const postResult = await this.hanldeResponseService.inECO(preResult);

      if (postResult) {
        await this.connection.collection(this.collectionClient).findOneAndUpdate({ CODE: sc.client_main_mobile_app_code }, { $inc: { CALL_SERVICE_COUNT: 1 } }, { upsert: false });
      }

      return {
        session_key: postResult.session_key,
        device: postResult.device,
        decrypted_data: postResult.decrypted_data,
        user: postResult.user,
      };
    } catch (error: any) {
      throw error;
    }
  }

  // TODO: fix any type
  async outMSB(serviceUrl: string, payload: any) {
    try {
      const { data } = payload;
      const encryptedData = encryptData(data, sc.msb_gateway_mk);

      const response = await this.httpService.axiosRef.request({
        url: serviceUrl,
        method: 'POST',
        data: {
          encrypted_data: encryptData,
          checksum: checkSum(encryptedData),
          client_code: 'CHS',
        },
        timeout: this.configService.get<number>('REQUEST_TIME_OUT'),
      });

      return response?.data;
    } catch (error: any) {
      throw error;
    }
  }

  async outECO(serviceUrl: string, payload: any, client_code: string) {
    try {
      const response = await this.httpService.axiosRef.request({
        method: 'POST',
        url: serviceUrl,
        timeout: this.configService.get<number>('REQUEST_TIME_OUT'),
        headers: { 'Content-type': 'application/json' },
        data: {
          data: payload,
          checksum: checkSum(JSON.stringify(payload)),
          client_code: client_code,
        },
      });

      return response?.data;
    } catch (error: any) {
      console.log(error.message);

      throw error;
    }
  }

  async sendEventToCDP(payload: PayloadSendEventToCDP) {
    const { cif_number, channel_user_id, channel_journey, journey_story, story_screen_function, description, additional_data } = payload;

    try {
      const response = await this.httpService.axiosRef.request({
        method: 'POST',
        data: {
          cif_number: cif_number ? cif_number : '',
          channel__id: sc.cdp.channel__id,
          channel_user_id,
          channel_journey,
          journey_story,
          story_screen_function,
          description: description ? description : 'mplus',
          additional_data: additional_data ? additional_data : {},
        },
        timeout: this.configService.get<number>('REQUEST_TIME_OUT'),
        headers: {
          'Content-type': 'application/json',
          apikey: sc.external_apim.api_key,
        },
      });

      return response?.data;
    } catch (error: any) {
      throw error;
    }
  }
}
