import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HandleRequestService } from './handle-request.service';
import { Client, ClientSchema } from 'src/schemas/client.schema';
import { ConfigModule } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';
import { HandleResponseModule } from 'src/handle-response/handle-response.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Client.name,
        schema: ClientSchema,
      },
    ]),
    ConfigModule,
    HttpModule,
    HandleResponseModule,
  ],
  providers: [HandleRequestService],
  exports: [HandleRequestService],
})
export class HandleRequestModule {}
