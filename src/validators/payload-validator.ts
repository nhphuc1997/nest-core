import { IsNotEmpty } from 'class-validator';

export class PayloadCreateBill {
  @IsNotEmpty()
  request_data: {
    body: {
      card_token?: string;
      trans_date?: string;
      trans_id?: string;
      amount?: string;
      bill_number?: string;
      additional_data?: string;
      object_code?: string;
      PAYLOAD?: object;
    };
  };

  @IsNotEmpty()
  merchant_code: string;

  @IsNotEmpty()
  payment_code: string;
}

export class PayloadSaveTransactionToDb extends PayloadCreateBill {
  @IsNotEmpty()
  resultBill: Record<string, any>;
}

export class PayloadCreateBillBookingVNA {
  @IsNotEmpty()
  user: any; // TODO FIXME: any type

  @IsNotEmpty()
  session_key: string;

  @IsNotEmpty()
  decrypted_data: Record<string, any>;

  @IsNotEmpty()
  card_token: string;
}

export class PayloadSendEventToCDP {
  @IsNotEmpty() channel__id: string;
  @IsNotEmpty() channel_user_id: string;
  @IsNotEmpty() channel_journey: string;
  @IsNotEmpty() journey_story: string;
  @IsNotEmpty() story_screen_function: string;
  cif_number: string;
  description: string | null;
  additional_data: object | null;
}

export class PayloadCreateBillFlightBookingVNA {
  @IsNotEmpty() encrypted_data: string;
  @IsNotEmpty() checksum: string;
  @IsNotEmpty() device_unique_id: string;
}
