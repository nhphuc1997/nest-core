import { Injectable, NestInterceptor, ExecutionContext, CallHandler, BadRequestException, BadGatewayException, NotFoundException } from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ErrorsInterceptor implements NestInterceptor {
  intercept(_context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      catchError((err: any) => {
        if (err?.response?.status === 404) return throwError(() => new NotFoundException());
        if (err?.status === 400) return throwError(() => new BadRequestException(err?.response?.message));

        if (err?.statusCode === 404) return throwError(() => new NotFoundException());
        if (err?.statusCode === 502) return throwError(() => new BadGatewayException());
        return throwError(() => new BadRequestException());
      }),
    );
  }
}
