import { Module } from '@nestjs/common';
import { CashOutService } from './cash-out.service';
import { CashOutController } from './cash-out.controller';
import { CreateBookingFlightModule } from 'src/create-booking-flight/create-booking-flight.module';

@Module({
  imports: [CreateBookingFlightModule],
  controllers: [CashOutController],
  providers: [CashOutService],
})
export class CashOutModule {}
