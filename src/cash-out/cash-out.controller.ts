import { Body, Controller, Post, UseInterceptors } from '@nestjs/common';
import { CreateBookingFlightService } from 'src/create-booking-flight/create-booking-flight.service';
import { ErrorsInterceptor } from 'src/interceptors/error.interceptor';
import { SuccessInterceptor } from 'src/interceptors/sucess.interceptor';
import { PayloadCreateBillFlightBookingVNA } from 'src/validators';

@Controller('payment')
export class CashOutController {
  constructor(private readonly createBookingService: CreateBookingFlightService) {}

  @UseInterceptors(new ErrorsInterceptor())
  @UseInterceptors(new SuccessInterceptor())
  @Post('create_bill_flight_booking_vna')
  createBillFlightBookingVNA(@Body() payload: PayloadCreateBillFlightBookingVNA) {
    return this.createBookingService.bookingVNA(payload);
  }
}
