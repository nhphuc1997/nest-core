import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { winstonInstance } from 'src/loggers';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const configService = new ConfigService();
  const PORT = configService.get<number>('PORT');
  const NODE_ENV = configService.get<string>('NODE_ENV');

  if (NODE_ENV === 'production') app.useLogger(winstonInstance);
  app.useGlobalPipes(new ValidationPipe());

  await app.listen(PORT || 3001);
}

bootstrap();
