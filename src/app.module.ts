import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PaymentModule } from './payment/payment.module';
import { HandleRequestModule } from './handle-request/handle-request.module';
import { MongooseModule } from '@nestjs/mongoose';
import { dbConfig } from './mongodb/db-config';
import { HandleResponseModule } from './handle-response/handle-response.module';
import { CreateBookingFlightModule } from './create-booking-flight/create-booking-flight.module';
import { CashOutModule } from './cash-out/cash-out.module';

@Module({
  imports: [
    PaymentModule,
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
    HandleRequestModule,
    MongooseModule.forRoot(dbConfig.url),
    HandleResponseModule,
    CreateBookingFlightModule,
    CashOutModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
