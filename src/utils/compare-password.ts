import * as bcrypt from 'bcrypt';

export const comparePassword = (pwd1: string, pwd2: string): boolean => {
  return bcrypt.compareSync(pwd1, pwd2);
};
