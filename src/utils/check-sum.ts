const md5 = require('md5');

export const checkSum = (data: string): string => {
  return md5(data);
};
