import AES from 'aes-js';

const buf2hex = (buffer: any) => {
  return Array.prototype.map.call(new Uint8Array(buffer), (x: any) => ('00' + x.toString(16)).slice(-2)).join('');
};

export const decryptData = (encryptedData: string, rawKey: string) => {
  const cipherIV = rawKey.substring(0, 16);
  const token = rawKey.substr(0, 32);
  const iv = AES.utils.utf8.toBytes(cipherIV);
  const key = AES.utils.utf8.toBytes(token);
  const aes_cbc = new AES.ModeOfOperation.cbc(key, iv);
  const decryptedByteData = aes_cbc.decrypt(AES.utils.hex.toBytes(encryptedData));
  const unpadData = AES.padding.pkcs7.strip(decryptedByteData);

  return AES.utils.utf8.fromBytes(unpadData);
};

export const encryptData = (rawData: any, rawKey: string) => {
  const cipherIV = rawKey.substring(0, 16);
  const token = rawKey.substring(0, 32);
  const iv = AES.utils.utf8.toBytes(cipherIV);
  const key = AES.utils.utf8.toBytes(token);
  const aesCbc = new AES.ModeOfOperation.cbc(key, iv);
  const byte_data = AES.utils.utf8.toBytes(JSON.stringify(rawKey));
  const padded_data = AES.padding.pkcs7.pad(byte_data);
  const encryptedByteData = aesCbc.encrypt(padded_data);

  return buf2hex(encryptedByteData);
};
