export interface ErrorInterface {
  statusCode: number;
  message: string | Array<string>;
  error: string;
}
