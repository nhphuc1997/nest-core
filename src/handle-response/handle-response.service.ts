import { Injectable } from '@nestjs/common';
import { checkSum, decryptData, encryptData } from 'src/utils';
import * as sc from 'src/configs/service_config.json';

@Injectable()
export class HandleResponseService {
  // TODO: fixme: fix any type
  inMSB(responseData: any) {
    const { checksum, encrypted_data } = responseData;
    const verifyCheckSum = checkSum(encrypted_data) === checksum;
    if (verifyCheckSum === false) return null;
    const responseDecryptedData = decryptData(encrypted_data, sc.msb_gateway_mk);
    return JSON.parse(responseDecryptedData);
  }

  inECO(responseData: any) {
    const { checksum, data } = responseData;
    const verifyCheckSum = checkSum(data) === checksum;
    if (verifyCheckSum) {
      if (data?.msg_code === 200) return data?.msg_data || data?.msg_code;
      return null;
    }
    return null;
  }

  mobileAppBasic(responseData: any, session_key: string) {
    const encrypted_data = encryptData(responseData, session_key);
    return {
      encrypted_data: encrypted_data,
      checksum: checkSum(encrypted_data),
    };
  }
}
