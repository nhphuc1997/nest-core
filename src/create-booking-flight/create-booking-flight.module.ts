import { Module } from '@nestjs/common';
import { HandleRequestModule } from 'src/handle-request/handle-request.module';
import { HandleResponseModule } from 'src/handle-response/handle-response.module';
import { PaymentModule } from 'src/payment/payment.module';
import { CreateBookingFlightService } from './create-booking-flight.service';

@Module({
  imports: [HandleRequestModule, HandleResponseModule, PaymentModule],
  providers: [CreateBookingFlightService],
  exports: [CreateBookingFlightService],
})
export class CreateBookingFlightModule {}
