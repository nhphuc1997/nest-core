import { Injectable } from '@nestjs/common';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { HandleRequestService } from 'src/handle-request/handle-request.service';
import { HandleResponseService } from 'src/handle-response/handle-response.service';
import { PaymentService } from 'src/payment/payment.service';
import { PayloadCreateBillFlightBookingVNA } from 'src/validators';
import * as sc from 'src/configs/service_config.json';
import { v4 as uuidv4 } from 'uuid';
import { dbConfig } from 'src/mongodb/db-config';
import { PayloadSendEventToCDP } from 'src/validators/payload-validator';

@Injectable()
export class CreateBookingFlightService {
  private readonly collectionFlightBookingHistory = dbConfig.collection.FLIGHT_BOOKING_HISTORY;
  private readonly vnaBookingFlightUrl = `${sc.community_service.host}/community/vietnam_airline_online/book_flight`;
  private readonly createBillUrl = `${sc.msb_gateway_url}/msb_gateway/payment/create-bill`;
  private readonly verifyTransactionUrl = `${sc.msb_gateway_url}/msb_gateway/payment/verify-transaction`;
  private readonly revertTransactionUrl = `${sc.msb_gateway_url}/msb_gateway/payment/revert-transaction`;

  constructor(
    @InjectConnection()
    private readonly connection: Connection,
    private readonly handleRequestService: HandleRequestService,
    private readonly hanldeResponseService: HandleResponseService,
    private readonly paymentService: PaymentService,
  ) {}

  private async saveFlightToDb(payload: any) {
    const codeFlight = uuidv4();
    const { flightBody, user } = payload;

    await this.connection.collection(this.collectionFlightBookingHistory).insertOne({
      CUSTOMER__CODE: user.CODE,
      CODE: codeFlight,
      PASSENGERS: flightBody.passengers,
      BAGGAGE: flightBody.baggage,
      BOOKINGCODE: flightBody.bookingCode,
      CONTACT: flightBody.contact,
      INSURRANCE_CODE: flightBody.insurrance_code,
      PRICES: flightBody.prices,
      FLIGHTS: flightBody.flights,
      FLIGHTS_DETAILS: flightBody.flights_detail,
      CUSTOMERINFOS: flightBody.customerInfos,
      CREATED_AT: new Date(),
      STATUS: 1, //khoi tao chua thanh toan.
      AMOUNT: flightBody.amount,
      DISCOUNT_CODE: flightBody.discount_code,
      PARTNER: 'VNA',
    });

    return codeFlight;
  }

  async bookingVNA(payload: PayloadCreateBillFlightBookingVNA) {
    try {
      const data = await this.handleRequestService.requestCustomer(payload);

      const { user, session_key, decrypted_data } = data;
      const card_token = decrypted_data.card_token;

      const preResult = await this.handleRequestService.outECO(this.vnaBookingFlightUrl, decrypted_data, 'CPS');
      const postResult = await this.hanldeResponseService.inECO(preResult);

      if (postResult?.code === '00') {
        const dataFromBookingService = postResult.data;
        const flightBody = {
          passengers: decrypted_data.passengers,
          baggage: decrypted_data.baggage,
          bookingCode: decrypted_data.flights_detail[0].ticket.list_segment[0].bookingCode,
          contact: decrypted_data.contact,
          insurrance_code: decrypted_data.insurrance_code,
          prices: decrypted_data.prices,
          flights: decrypted_data.flights,
          flights_detail: decrypted_data.flights_detail,
          customerInfos: decrypted_data.flights_detail[0].customers,
          discount_code: decrypted_data.discount_code,
          amount: dataFromBookingService.fairInfo.totalFair,
        };
        const flightCode = await this.saveFlightToDb(flightBody);

        const createBillBody = {
          bookingId: '', // TODO: FIXME: ambition booking Id
          bill_number: uuidv4(),
          card_token: card_token,
          trans_id: dataFromBookingService.transId,
          merchant_name: 'MSB PLus',
          amount: dataFromBookingService.fairInfo.totalFair,
          currency: 'VND',
          merchant_code: 'vna',
          card_name: user?.FULLNAME,
          expired_date: decrypted_data.expired_date,
          object_code: flightCode,
        };

        await this.paymentService.makeRequestToMSBGateWay(
          {
            request_data: { body: createBillBody },
            merchant_code: 'vna',
            payment_code: decrypted_data.payment_type,
          },
          this.createBillUrl,
        );

        this.handleRequestService.sendEventToCDP({
          cif_number: user.MSB_CLIENT_NUMBER,
          channel_user_id: user.CODE,
          channel_journey: 'customer_buy_airline_tickets',
          journey_story: 'customer_submit_information',
          story_screen_function: 'submit_information_screen',
          description: null,
          additional_data: null,
        } as PayloadSendEventToCDP);

        const reponseToMobileApp = {
          msg_description: `Consumer create flight booking bill for debit card payment successfully`,
          msg_data: {
            trans_id: createBillBody.trans_id,
            order_code: createBillBody.bookingId,
          },
        };

        return this.hanldeResponseService.mobileAppBasic(reponseToMobileApp, session_key);
      }
    } catch (error: any) {
      throw error;
    }
  }
}
