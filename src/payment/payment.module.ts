import { Module } from '@nestjs/common';
import { PaymentService } from './payment.service';
import { HttpModule } from '@nestjs/axios';
import { ConfigModule } from '@nestjs/config';
import { HandleRequestModule } from 'src/handle-request/handle-request.module';
import { HandleResponseModule } from 'src/handle-response/handle-response.module';

@Module({
  imports: [HttpModule, ConfigModule, HandleRequestModule, HandleResponseModule],
  providers: [PaymentService],
  exports: [PaymentService],
})
export class PaymentModule {}
