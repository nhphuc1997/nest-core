import { Injectable, Logger } from '@nestjs/common';
import { HandleRequestService } from 'src/handle-request/handle-request.service';
import { HandleResponseService } from 'src/handle-response/handle-response.service';
import { InjectConnection } from '@nestjs/mongoose';
import { Connection } from 'mongoose';
import { dbConfig } from 'src/mongodb/db-config';

@Injectable()
export class PaymentService {
  private readonly logger = new Logger(PaymentService.name);
  private collectionInAppTransaction = dbConfig.collection.CLIENT;

  constructor(
    @InjectConnection()
    private readonly connection: Connection,
    private readonly handleRequestService: HandleRequestService,
    private readonly handleResponseService: HandleResponseService,
  ) {}

  // TODO: FIXME: any type
  private async saveTransactionToDB(payload: any) {
    const { body } = payload.request_data;

    return await this.connection.collection(this.collectionInAppTransaction).insertOne({
      TYPE: 'partner',
      PARTNER: payload.merchant_code,
      CARD_TOKEN: body.card_token,
      TRANS_DATE: body.trans_date,
      TRANS_ID: body.trans_id,
      AMOUNT: body.amount,
      BILL_NUMBER: body.bill_number,
      ADDITIONAL_INFORMATION: body.additional_data,
      PAYLOAD: body.PAYLOAD,
      OBJECT__CODE: body.object_code,
      PAYMENT_TYPE: payload.payment_type,
      TRANS_BILL_ID: payload.resultBill.msg_data.transId,
      CREATED_AT: new Date(),
      STATUS: 1,
    });
  }

  async makeRequestToMSBGateWay(payload: any, url: string, isSaveToDb = false) {
    try {
      const responseOutMsb = await this.handleRequestService.outMSB(url, payload);
      const reponseInMsb = this.handleResponseService.inMSB(responseOutMsb);

      if (reponseInMsb && reponseInMsb?.msg_code === 200 && isSaveToDb) {
        await this.saveTransactionToDB({
          ...payload,
          resultBill: reponseInMsb,
        });
      }

      return reponseInMsb;
    } catch (error: any) {
      this.logger.error(`[payment]:::[error]:::${error?.message}`);
      throw error;
    }
  }
}
